import sqlite3
import uvicorn
from fastapi import FastAPI, Request, status
from fastapi import FastAPI, Request, status, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

conn = sqlite3.connect(r"C:\Users\user\Desktop\Учеба\прога\ар\123\uyyu.db", check_same_thread=False)
app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

@app.post("/sickness ")
def fetch_data(id_d: int, desease: str):
    #insert_query = f"INSERT INTO  sickness(id_d, desease) VALUES (?,?)", (id_d, desease)
    cursor = conn.execute(f"INSERT INTO  sickness(id_d, desease) VALUES (?,?)", (id_d, desease))
    results = conn.commit()

@app.get("/")
def info(request: Request):
    coursor = str("Add: /sikcness or /Treatment")
    return templates.TemplateResponse("1.html", {"coursor": coursor, "request": request})

@app.get("/sikcness") 
def get_users(request: Request):
    cursor = conn.execute("SELECT * FROM sikcness;")
    result = [
        {
            "id_d": sikcness[0], "disease": sikcness[1]
        } for sikcness in cursor.fetchall()
    ]
    return templates.TemplateResponse("sikcness.html", {"result": result, "request": request})

@app.get("/Treatment")
def get_names(request: Request):
    cursor = conn.execute("SELECT * FROM Treatment;")
    result = [
        {
            "id": Treatment[0], "Medicine": Treatment[1]
        } for Treatment in cursor.fetchall()
    ]
    return templates.TemplateResponse("Treatment.html", {"result": result, "request": request})


@app.patch("/Treatment /{id}", status_code=status.HTTP_200_OK)
def update_user(id: int, Medicine: str):
    cursor = conn.execute(f"SELECT * FROM Treatment  WHERE Treatment .id={id};")
    query_result = cursor.fetchone()
    if not query_result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="user not found"
        )
    conn.execute(f"UPDATE Treatment SET id = ('{id}'), Medicine = ('{Medicine}')")
    conn.commit()

@app.delete("/Treatment/{id}")
def delete_user(id: int):
    cursor = conn.execute(f"SELECT * FROM Treatment WHERE Treatment.id={id};")
    user = cursor.fetchone()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="user not found"
        )
    conn.execute(f"DELETE FROM Treatment WHERE id={id}")

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8080)
