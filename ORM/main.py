from typing import List, Optional

import uvicorn
from fastapi import FastAPI, HTTPException
from sqlmodel import Field, SQLModel, select, create_engine, Session
from starlette import status

connect_args = {"check_same_thread": False}
engine = create_engine("sqlite:///uyyu.db", echo=True,
                       connect_args=connect_args)

app = FastAPI()


class sicknessBase(SQLModel):
    id_d: int
    disease: str
    


class sickness(sicknessBase, table=True):
    id_d: Optional[int] = Field(default=None, primary_key=True)
    disease: Optional[str] = Field(default=None, primary_key=False)
 

class sicknessToGet(sicknessBase):
    id_d: int
    disease: str


class sicknessToCreate(sicknessBase):
    id_d: int
    disease: str


class sicknessToUpdate(sicknessBase):

    pass


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


@app.on_event("startup")
def on_startup():
    create_db_and_tables()


@app.get("/sickness", response_model=List[sickness])  # 127.0.0.1:8080/sickness
def get_sickness() -> List[sickness]:
    with Session(engine) as session:
        disease: List[sickness] = session.exec(select(sickness)).all()
    return disease


@app.post("/sickness", status_code=201)
def add_sickness(user: sicknessToCreate):
    with Session(engine) as session:
        db_uyyu = sickness.from_orm(user)
        session.add(db_uyyu)
        session.commit()
        session.refresh(db_uyyu)
        return db_uyyu


@app.patch("/sickness/{sickness_id}", status_code=status.HTTP_200_OK)
def update_sickness(sickness_id: int, disease: sicknessToUpdate):
    with Session(engine) as session:
        db_uyyu = session.get(sickness, sickness_id)
        if not db_uyyu:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="sickness not found")
        db_uyyu.disease = disease.disease
        session.add(db_uyyu)
        session.commit()
        session.refresh(db_uyyu)
        return db_uyyu


@app.delete("/sickness/{sickness_id}")
def delete_sickness(sickness_id: int):
    with Session(engine) as session:
        disease = session.get(sickness, sickness_id)
        if not disease:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="sickness not found")
        session.delete(disease)
        session.commit()
        return disease


@app.get("/sickness/{sickness_id}")
def get_sickness(sickness_id: int) -> Optional[sickness]:
    with Session(engine) as session:
        disease = session.get(sickness, sickness_id)
        if not disease:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="sickness not found")
        return disease


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8080)  # 127.0.0.1:8080
